// ==UserScript==
// @name       Stack Exchange Chat Room Auto-Expander
// @namespace  http://github.com/oliversalzburg
// @version    0.1
// @description  Automatically expands the room list in Stack Exchange chat.
// @match      http://chat.stackexchange.com/rooms/*
// @copyright  2014, Oliver Salzburg <oliver.salzburg@gmail.com>
// ==/UserScript==

setTimeout( function() {
	$( "#sidebar-content .more" ).click();
	$console.log( "Expanded room list." );
}, 1000 );
